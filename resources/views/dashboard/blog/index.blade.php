   @extends('dashboard.layouts.master')

   @section('admincontent')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">


             @if(Session::has('edit_msg'))
                <div class="alert alert-success">
                    {{Session::get('edit_msg') }}
                </div>
            @endif

            @if(Session::has('update_msg'))
                <div class="alert alert-warning">
                    {{Session::get('update_msg') }}
                </div>
            @endif


            @if(Session::has('delete_msg'))
                <div class="alert alert-danger">
                    {{Session::get('delete_msg') }}
                </div>
            @endif


            @foreach($blogs as $blog)

              <div class="post-preview">

              <!-- <table class="table table-condensed">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                  </tr>
                  <tr>
                    <td>Mary</td>
                    <td>Moe</td>
                    <td>mary@example.com</td>
                  </tr>
                  <tr>
                    <td>July</td>
                    <td>Dooley</td>
                    <td>july@example.com</td>
                  </tr>
                </tbody>
              </table> -->
                     <a>
                        <h2 class="post-title">
                           {{$blog->title}}
                        </h2>
                        <div class="pull-right btn-group">
                            <small><a class="btn btn-xs btn-warning" href="article/edit/{{$blog->id}}">Edit</a>
                            </small>
                             <small><a class="btn btn-xs btn-danger" href="article/destroy/{{$blog->id}}">Delete
                            </a> </small>
                      
                        </div>
                        
                        <h3 class="post-subtitle">
                             {{$blog->description}}
                        </h3>
                      
                             @if(!empty($blog->image))

                             <img src="/uploads/{{$blog->image}}" alt="" width="70px">
                            
                            @endif
                        
                    </a>
                
                </div>
            @endforeach
              <hr>


            </div>
        </div>
    </div>
<!--  -->
    <hr>

    @stop
    

